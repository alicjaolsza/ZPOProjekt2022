package com.zpo.lista1.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerTokenValidator {

    public static Customer validateToken(String customerToken, int... serviceIds) throws InvalidServiceException, InvalidTokenFormatException, InvalidCountryCodeFormatException {

        Pattern p = Pattern.compile("[A-Z]{2}[0-9]{4}" +
                "ex_ [0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}" +
                "[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+");

        Matcher m = p.matcher(customerToken);

        if (!m.matches()){
            throw new InvalidTokenFormatException("Token does not match expected pattern.");
        }

        Customer c = new Customer();

        c.setCountryCode(customerToken.substring(0,2));
        c.setClientId(Integer.parseInt(customerToken.substring(2,6)));
        c.setExternalId(customerToken.substring(10,46));

        String [] numbers = customerToken.substring(46).split("\\.");

        c.getEnabledServices().add(Integer.parseInt(numbers[0]));

        int serviceId = c.getEnabledServices().get(0);

        boolean validService = false;

        for (int id: serviceIds){
            if (id == serviceId){
                validService = true;
                break;
            }
        }

        if (!validService){
            throw new InvalidServiceException("Invalid Service ID.");
        }

        return c;
    }
}

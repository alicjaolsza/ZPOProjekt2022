package com.zpo.lista1.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountryCode {

    private String countryCode;

    public CountryCode(String countryCode) throws InvalidTokenFormatException, InvalidCountryCodeFormatException {
        validateCountryCode(countryCode);
        this.countryCode = countryCode;

    }

    public CountryCode() {
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) throws InvalidTokenFormatException, InvalidCountryCodeFormatException {
        validateCountryCode(countryCode);
        this.countryCode = countryCode;
    }

    public void validateCountryCode(String countryCode) throws InvalidTokenFormatException, InvalidCountryCodeFormatException {
        Pattern p = Pattern.compile("[A-Z]{2}");
        Matcher m = p.matcher(countryCode);

        if (!m.matches()){
            throw new InvalidCountryCodeFormatException("Country code does not match expected pattern.");
        }
    }
}

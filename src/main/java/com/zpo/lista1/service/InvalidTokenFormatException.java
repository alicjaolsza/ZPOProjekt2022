package com.zpo.lista1.service;

public class InvalidTokenFormatException extends Exception {

    public InvalidTokenFormatException(String errorMessage) {
        super(errorMessage);
    }
}

package com.zpo.lista1.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Customer {

    //public String countryCode;
    public CountryCode countryCode = new CountryCode();
    public long clientId;
    public UUID externalId;
    public List<Integer> enabledServices = new ArrayList<>();


    public Customer() {
    }

    public Customer(String countryCode, long clientId, String externalId, String enabledServices, int serviceId) throws InvalidTokenFormatException, InvalidCountryCodeFormatException {
        this.countryCode.setCountryCode(countryCode);
        this.clientId = clientId;
        this.externalId = UUID.fromString(externalId);
        this.enabledServices.add(serviceId);

    }

    public String getCountryCode() {
        return countryCode.getCountryCode();
    }

    public void setCountryCode(String countryCode) throws InvalidTokenFormatException, InvalidCountryCodeFormatException {
        this.countryCode.setCountryCode(countryCode);
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public UUID getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = UUID.fromString(externalId);
    }

    public List<Integer> getEnabledServices() {
        return enabledServices;
    }

    public void setEnabledServices(List<Integer> enabledServices) {
        this.enabledServices = enabledServices;
    }

    @Override
    public String toString() {
        return "com.zpo.lista1.service.Customer{" +
                "countryCode='" + countryCode + '\'' +
                ", clientId=" + clientId +
                ", externalId='" + externalId + '\'' +
                ", enabledServices='" + enabledServices + '\'' +
                '}';
    }
}

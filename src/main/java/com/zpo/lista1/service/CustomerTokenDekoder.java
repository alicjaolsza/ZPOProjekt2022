package com.zpo.lista1.service;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.Base64;


public class CustomerTokenDekoder extends Customer {


    public static Customer decode(String inputToken) throws InvalidTokenFormatException, InvalidServiceException, ParseException, InvalidCountryCodeFormatException {

        String[] JWTparts = inputToken.split("\\.");

        Base64.Decoder decoder = Base64.getUrlDecoder();

        String body = new String(decoder.decode(JWTparts[1]));

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(body);

        if (!json.containsKey("customerToken")) {
            throw new InvalidTokenFormatException("JWT body has no customer token.");
        }

        String customerToken = String.valueOf(json.get("customerToken"));

        Customer c = CustomerTokenValidator.validateToken(customerToken,ServiceConfig.serviceId);

        return c;
    }
}

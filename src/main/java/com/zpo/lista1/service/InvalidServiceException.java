package com.zpo.lista1.service;

public class InvalidServiceException extends Exception {
    public InvalidServiceException(String errormessage) {
        super(errormessage);
    }
}

package com.zpo.lista1.service;

public class InvalidCountryCodeFormatException extends Exception {
    public InvalidCountryCodeFormatException(String errormessage) {
        super(errormessage);
    }
}

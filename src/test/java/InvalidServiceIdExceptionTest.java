import com.zpo.lista1.service.CustomerTokenValidator;
import com.zpo.lista1.service.InvalidServiceException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InvalidServiceIdExceptionTest {

    private String customerToken = "PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1";

    @Test
    void invalidServiceIdExceptionTest(){

        Throwable exception = assertThrows(InvalidServiceException.class, () -> {
            CustomerTokenValidator.validateToken(customerToken, 15);
        });
        assertEquals("Invalid Service ID.", exception.getMessage());
    }
}

import com.zpo.lista1.service.Customer;
import com.zpo.lista1.service.CustomerTokenValidator;
import com.zpo.lista1.service.ServiceConfig;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class CustomerTokenValidatorTest {

    private String customerToken = "PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1";

    @Test
    void newCustomerObjectTest(){

        try {
            Customer c = CustomerTokenValidator.validateToken(customerToken, ServiceConfig.serviceId);
            assertEquals( "PL", c.getCountryCode());
        }
        catch (Exception e) {
            fail("com.zpo.lista1.service.Customer object not created.");
        }


    }
}

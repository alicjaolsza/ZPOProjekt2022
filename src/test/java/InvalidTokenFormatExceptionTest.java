import com.zpo.lista1.service.CustomerTokenValidator;
import com.zpo.lista1.service.InvalidTokenFormatException;
import com.zpo.lista1.service.ServiceConfig;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InvalidTokenFormatExceptionTest {

    private String invalidCustomerToken = "P14325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1";

    @Test
    void invalidTokenFormatExceptionTest(){


        Throwable exception = assertThrows(InvalidTokenFormatException.class, () -> {
            CustomerTokenValidator.validateToken(invalidCustomerToken, ServiceConfig.serviceId);
        });
        assertEquals("Token does not match expected pattern.", exception.getMessage());
    }

}

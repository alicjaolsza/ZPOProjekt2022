import com.zpo.lista1.service.*;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

public class CustomerTokenDekoderTest {

    private String validToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMG" +
            "QwLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0ZTEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIs" +
            "ImlhdCI6MTUxNjIzOTAyMn0.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g";

    @Test
    void decodingTest(){
        try {
            Customer c = CustomerTokenDekoder.decode(validToken);
        } catch (InvalidTokenFormatException e) {
            fail("com.zpo.lista1.service.Customer object not created.");
        } catch (InvalidServiceException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (InvalidCountryCodeFormatException e) {
            e.printStackTrace();
        }
    }
}
